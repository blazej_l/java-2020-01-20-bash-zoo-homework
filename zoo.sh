#!/usr/bin/env bash

<<ZOO
The program shows the number of catalogs in catalog ./animals

The program shows the names of the catalogs in catlog ./animals
and the number of files in each of them

The program shows the names of the catalogs in catalog ./animals
and the file names in each of them
ZOO

#TASK 1

echo "Task 1"
echo -n "Number of species: "
number=$(find animals/* -type d | wc -w)
echo "${number}"
echo "..."

#TASK 2

echo "Task 2"
names=($(ls animals/))
size=${#names[@]}

for ((i = 0; i < size; i++))
do
	echo "${names[i]}: $(find animals/${names[i]}/* -type f | wc -w)"

done
echo "..."

#TASK 3

echo "Task 3"
for ((i = 0; i < size; i++))
do
	echo "${names[i]}: $(ls animals/${names[i]}/)"
done


